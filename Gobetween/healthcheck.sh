#!/usr/bin/env bash

response=$(curl -Is localhost | head -n 1)
expected="HTTP/1.1 200 OK"

if [ "$(echo $response | grep 'HTTP/1.1 200 OK')" != "" ];
then echo -n 1
else echo -n 0
fi
